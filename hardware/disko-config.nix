{
  disko.devices = {
    disk = {
      system = {
        type = "disk";
        device = "/dev/nvme0n1";
        content = {
          type = "gpt";
          partitions = {
            ESP = {
              size = "512M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
              };
            };
            root = {
              size = "100%";
              content = {
                type = "btrfs";
                extraArgs = [ "-f" ];
                subvolumes = {
                  "@/" = {
                    mountOptions = [ "compress=zstd" "discard=async" "noatime" ];
                    mountpoint = "/";
                  };
                  "@/home" = {
                    mountOptions = [ "compress=zstd" "discard=async" "noatime" ];
                    mountpoint = "/home";
                  };
                  "@/nix" = {
                    mountOptions = [ "compress=zstd" "discard=async" "noatime" ];
                    mountpoint = "/home";
                  };
                  "@/swap" = {
                    mountOptions = [ "noatime" "nodatacow" "discard=async" ];
                    mountpoint = "/.swapvol";
                    swap =
                    {
                      swap-0.size = "36G";
                      swap-0.path = "swap-0";
                    };
                  };
                };
              };
            };
          };
        };
      };
    };
  };
}
