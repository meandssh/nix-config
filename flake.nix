{
  description = "An immature NixOS flake config";

  inputs = {
    hardware.url = "github:nixos/nixos-hardware";
    hyprland.url = "github:hyprwm/Hyprland";
    impermanence.url = "github:nix-community/impermanence";
    nixd.url = "github:nix-community/nixd";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-23.11";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    { self
    , home-manager
    , nixd
    , nixpkgs
    , nixpkgs-stable
    , ...
    }@inputs:
    let
      hostname = "RMG21N";
      system = "x86_64-linux";
      specialArgs = {
        inherit inputs;
        pkgs-stable = import nixpkgs-stable {
          system = system;
          config.allowUnfree = true;
        };
      };
    in
    {
      nixosConfigurations."${hostname}" = nixpkgs.lib.nixosSystem rec {
        modules = [
          {
            nixpkgs.overlays = [ nixd.overlays.default ];
          }
          ./configuration.nix
          home-manager.nixosModules.home-manager
          {
            home-manager = {
              useGlobalPkgs = true;
              users.meandssh = import ./home;
              useUserPackages = true;
              extraSpecialArgs = specialArgs;
            };
          }
        ];
      };
    };
}
