# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).
{ config, lib, pkgs, ... }: {
  imports = [
    ./hardware/disko-config.nix
    ./hardware/hardware-configuration.nix
    "${builtins.fetchTarball {
      url = "https://github.com/nix-community/disko/archive/refs/tags/v1.3.0.tar.gz"; 
      sha256 = "1lrnvgd5w41wrgidp3vwv2ahpvl0a61c2lai6qs16ri71g00kqn0";
    }}/module.nix"
  ];

  nix = {
    gc = {
      automatic = true;
      dates = "monthly";
      options = "--delete-older-than 1m";
    };
    package = pkgs.nixFlakes;
    settings = {
      auto-optimise-store = true;
      experimental-features = [ "nix-command" "flakes" ];
      substituters = lib.mkBefore [
        "https://hyprland.cachix.org"
        "https://nix-community.cachix.org"
        "https://mirrors.tuna.tsinghua.edu.cn/nix-channels/store"
      ];
      trusted-public-keys = [
        "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];
    };
  };

  nixpkgs = {
    config.allowUnfree = true;
    hostPlatform = lib.mkDefault "x86_64-linux";
  };

  networking = {
    firewall.enable = true;
    hostName = "RMG21N";
    networkmanager.enable = true;
    # proxy.default = "http://user:password@proxy:port/";
    # proxy.noProxy = "127.0.0.1,localhost,internal.domain";
  };

  time.timeZone = "Asia/Shanghai";

  i18n = {
    defaultLocale = "zh_CN.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS = "zh_CN.UTF-8";
      LC_IDENTIFICATION = "zh_CN.UTF-8";
      LC_MEASUREMENT = "zh_CN.UTF-8";
      LC_MONETARY = "zh_CN.UTF-8";
      LC_NAME = "zh_CN.UTF-8";
      LC_NUMERIC = "zh_CN.UTF-8";
      LC_PAPER = "zh_CN.UTF-8";
      LC_TELEPHONE = "zh_CN.UTF-8";
      LC_TIME = "zh_CN.UTF-8";
    };
    supportedLocales = [
      "zh_CN.UTF-8/UTF-8"
      "en_US.UTF-8/UTF-8"
    ];
    inputMethod = {
      enabled = "fcitx5";
      fcitx5 = {
        addons = with pkgs; [
          fcitx5-chinese-addons
          fcitx5-gtk
        ];
      };
    };
  };

  security.rtkit.enable = true;

  services = {
    desktopManager.plasma6.enable = true;
    openssh = {
      enable = true;
      openFirewall = true;
      settings = {
        PasswordAuthentication = false;
        PermitRootLogin = "no";
        X11Forwarding = true;
      };
    };
    pipewire = {
      enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      pulse.enable = true;
    };
    printing.enable = true;
    v2raya.enable = true;
    xserver = {
      enable = true;
      displayManager.sddm = {
        enable = true;
        wayland.enable = true;
      };

      xkb.layout = "cn";
      libinput.enable = true;
      videoDrivers = [ "nvidia" ];
    };
  };
  sound.enable = true;
  users.users.meandssh = {
    extraGroups = [ "dialout" "networkmanager" "wheel" ];
    isNormalUser = true;
    openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDlsCGdv9hr/OXwUvdynYxgjlFpDaMWfTOoLSIiZTUfy RMG21N meandSSH0219@gmail.com" ];
    shell = pkgs.nushellFull;
  };
  fonts = {
    fontDir.enable = true;
    packages = with pkgs; [
      material-design-icons
      (nerdfonts.override { fonts = [ "FiraCode" "JetBrainsMono" ]; })
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      source-han-sans
      source-han-serif
    ];
  };
  environment = {
    sessionVariables.NIXOS_OZONE_WL = "1";
    systemPackages = with pkgs; [
      bat
      curl
      fd
      file
      git
      lshw
      neovim
      nix-output-monitor
      nixd
      nixpkgs-fmt
      p7zip
      podman-compose
      ripgrep
      tealdeer
      tree
      unzip
      usbutils
      wget
      xz
      yazi
      zip
      zoxide
      zsh
    ];

    variables.EDITOR = "neovim";
  };
  qt = {
    enable = true;
    # platformTheme = "gnome";
    # style = "adwaita-dark";
  };
  system.stateVersion = "23.11";
  virtualisation = {
    libvirtd.enable = true;
    podman = {
      enable = true;
      dockerCompat = true;
      defaultNetwork.settings.dns_enabled = true;
    };
  };
  programs = {
    hyprland = {
      enable = true;
      package = inputs.hyprland.packages.${pkgs.system}.hyprland;
    };
    virt-manager.enable = true;
  };
}

