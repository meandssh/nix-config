{ config, pkgs, pkgs-stable, ... }: {
  imports = [
    ./nushell
    ./packages.nix
    ./starship.nix
  ];

  home = {
    stateVersion = "23.11";
    homeDirectory = "/home/meandssh";
    username = "meandssh";
  };

  # 递归将某个文件夹中的文件，链接到 Home 目录下的指定位置
  # home.file.".config/i3/scripts" = {
  #   source = ./scripts;
  #   recursive = true;   # 递归整个文件夹
  #   executable = true;  # 将其中所有文件添加「执行」权限
  # };

  # 直接以 text 的方式，在 nix 配置文件中硬编码文件内容
  # home.file.".xxx".text = ''
  #     xxx
  # '';

  # xresources.properties = {
  #   "Xcursor.size" = 16;
  #   "Xft.dpi" = 144;
  # };

  programs = {
    git = {
      enable = true;
      userName = "meandSSH";
      userEmail = "meandSSH0219@gmail.com";
      extraConfig = {
        init.defaultBranch = "main";
      };
    };
    home-manager.enable = true;
  };
  wayland.windowManager.hyprland = {
    enable = true;
    xwayland.enable = true;
    systemd.enable = true;
  };
}
