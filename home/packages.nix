{ config, pkgs, pkgs-stable, ... }: {
  home.packages = with pkgs; [
    atuin
    bacon
    direnv
    filelight
    firefox-devedition
    gh
    gitui
    hyfetch
    localsend
    microsoft-edge

    onedrivegui
    onefetch
    pods
    qq
    vscode
    winetricks
    wineWowPackages.stable
    wpsoffice-cn
    zotero_7
  ];
} 
