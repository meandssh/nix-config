{
  programs.starship = {
    enable = true;
    settings = {
      # "$schema" = "https://starship.rs/config-schema.json";
      add_newline = true;
      command_timeout = 100;
      continuation_prompt = "▶▶";

      format = "$os$shlvl[<](bold red)$username$hostname[>](bold red) $all[╰─](purple) $directory$character";
      right_format = "$battery$memory_usage$time";

      battery.display = [
        {
          threshold = 200;
          style = "green";
        }
        {
          threshold = 50;
          style = "yellow";
        }
        {
          threshold = 20;
          style = "red";
        }
      ];

      character = {
        success_symbol = "[~>](bold blink green)";
        error_symbol = "[✗](bold blink red)";
      };

      directory = {
        truncation_length = 10;
        truncate_to_repo = true; # truncates directory to root folder if in github repo
        format = "\\[[$path]($style)[$lock_symbol]($lock_style)\\] ";
      };

      memory_usage = {
        symbol = "💿";
        format = "$symbol[$ram(|$swap)]($style) ";
        threshold = -1;
        style = "dimmed bright-cyan";
        disabled = false;
      };

      os = {
        format = "[$symbol](purple) ";
        symbols.Windows = "";
        disabled = false;
      };

      shlvl = {
        disabled = false;
        threshold = -1;
        symbol = "↕️";
      };

      sudo = {
        style = "bold green";
        symbol = "💻";
        disabled = false;
      };

      time = {
        time_format = "%T";
        format = "🕙[$time]($style)";
        style = "bright-blue";
        disabled = false;
      };
      username = {
        style_user = "green bold";
        style_root = "red bold";
        format = "[$user]($style)";
        disabled = false;
        show_always = true;
      };
      hostname = {
        ssh_only = false;
        format = "[@](bold blink purple)[$hostname](bold yellow)";
        trim_at = ".";
        disabled = false;
      };
    };
  };
}
